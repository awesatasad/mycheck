package com.mycheck.recommendations.controller;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mycheck.recommendations.service.RecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
public class RecommendationController extends AbstractController {


    private static final String GET_RECOMMNDATIONS = "/get";
    private static final String GET_ALL_RECOMMNDATIONS = "/get-all";//get all reocmmenadation for recommendations


    public static class RecommendResult extends ResponseBaseObject {

        private final Long itemId;
        private final List<RecommendResponse> recommendationList;

        public RecommendResult(Long itemId, List<RecommendResponse> recommendationList) {
            this.itemId = itemId;
            this.recommendationList = recommendationList;
        }

        public Long getItemId() {
            return itemId;
        }

        public List<RecommendResponse> getRecommendationList() {
            return recommendationList;
        }
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class RecommendResponse {
        public Long itemID;
        public Double value;
    }


    public static class RecommendOfRecommendResult extends ResponseBaseObject {
        public List<RecommendResult> recommendationsOfRecommendationResult = new ArrayList<>();

    }


    @Autowired
    @Qualifier("recommend")
    protected RecommendService recommendService;


    @RequestMapping(value = GET_RECOMMNDATIONS, method = RequestMethod.GET)
    protected ResponseEntity<ResponseBaseObject> getRecommendationsItem(@RequestHeader HttpHeaders headers, @RequestParam Long itemId) throws Exception {

        return handleExceptionWrapper(() -> {
            if (headers == null || !headers.containsKey("token")) { // checking auth
                throw new Exception("Request body missing required Params.");
            }

            return new ResponseEntity<>(recommendService.getRecommendationsListForItem(headers, itemId), HttpStatus.OK);
        });

    }


    @RequestMapping(value = GET_ALL_RECOMMNDATIONS, method = RequestMethod.GET)
    protected ResponseEntity<ResponseBaseObject> getRecommendations(@RequestHeader HttpHeaders headers, @RequestParam Long itemId, @RequestParam Integer depth) throws Exception {

        return handleExceptionWrapper(() -> {
            if (headers == null || !headers.containsKey("token")) { // checking auth
                throw new Exception("Request body missing required Params.");
            }

            return new ResponseEntity<>(recommendService.RecommendationsForRecommendations(headers, itemId, depth), HttpStatus.OK);
        });

    }


}
