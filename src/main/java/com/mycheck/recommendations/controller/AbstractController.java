package com.mycheck.recommendations.controller;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.ResponseEntity;

import java.util.concurrent.Callable;



/*
* in our case it only 1 controller with 1 controller
* and this abstract with the common properties for all controllers
 */
public abstract class AbstractController {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ResponseBaseObject {

    }


    protected ResponseEntity<ResponseBaseObject> handleExceptionWrapper(Callable<ResponseEntity<ResponseBaseObject>> callable) throws Exception {

        ResponseEntity<ResponseBaseObject> result = null;
        try {
            result = callable.call();
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

}
