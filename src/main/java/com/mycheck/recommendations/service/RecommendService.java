package com.mycheck.recommendations.service;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mycheck.recommendations.controller.AbstractController;
import com.mycheck.recommendations.controller.RecommendationController;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.*;


@Service("recommend")
public class RecommendService {

    private static Gson gson = new Gson() ;

    public static class RecommendRsponseDepth {

        private final Integer depth;
        private final Long itemId;

        public RecommendRsponseDepth(Long itemId, Integer depth) {
            this.depth = depth;
            this.itemId = itemId;
        }

        public Integer getDepth() {
            return depth;
        }

        public Long getItemId() {
            return itemId;
        }

    }

    private static final String GET_RECOMMEND_ITEM_URL = "http://mycheckjavaexam-env.eu-west-1.elasticbeanstalk.com/api/v1/recommend/franco?item=%s";


    public RecommendationController.RecommendResult getRecommendationsListForItem(HttpHeaders headers, Long itemId) {

        RecommendationController.RecommendResult result = null;
        String requestUrl = String.format(GET_RECOMMEND_ITEM_URL, itemId);
        HttpGet httpGet = new HttpGet(requestUrl);
        HttpClient client = HttpClientBuilder.create().build();

        try {
            HttpResponse response = client.execute(httpGet);
            Type listType = new TypeToken<List<RecommendationController.RecommendResponse>>() {
            }.getType();
            InputStream is = response.getEntity().getContent();
            String jsonResponse = IOUtils.toString(is, "UTF-8");
            List<RecommendationController.RecommendResponse> recommendResponses = gson.fromJson(jsonResponse, listType);

            result = new RecommendationController.RecommendResult(itemId, recommendResponses);

        } catch (IOException e) {

        }
        return result;
    }

    public RecommendationController.RecommendOfRecommendResult RecommendationsForRecommendations(HttpHeaders headers, Long startItemId, int depth) {


        HashSet<Long> recommendItemsIds = new HashSet<>(); // prevent duplicate recommend Items
        recommendItemsIds.add(startItemId);

        RecommendationController.RecommendOfRecommendResult recommendOfRecommendResult = new RecommendationController.RecommendOfRecommendResult(); //wrapper response
        List<RecommendationController.RecommendResult> result = new ArrayList<>();// response

        Queue<RecommendRsponseDepth> recommendRsponseDepthsStack = new LinkedList<>();

        RecommendRsponseDepth recommendRsponseDepthRoot = new RecommendRsponseDepth(startItemId, 0);
        recommendRsponseDepthsStack.add(recommendRsponseDepthRoot);

        while (!recommendRsponseDepthsStack.isEmpty()) {
            RecommendRsponseDepth recommendRsponse = recommendRsponseDepthsStack.poll();

            if (recommendRsponse.getDepth() == depth) {// or  == depth + 1 dependence on which index you start (0 or 1 ) my first depth is 0
                break;
            }
            RecommendationController.RecommendResult individualReommendItemResult = getRecommendationsListForItem(headers, recommendRsponse.itemId);
            RecommendationController.RecommendResult recommendResult = new RecommendationController.RecommendResult(individualReommendItemResult.getItemId(), individualReommendItemResult.getRecommendationList());
            result.add(recommendResult);


            for (RecommendationController.RecommendResponse recommendResponse : individualReommendItemResult.getRecommendationList()) {

                if (!recommendItemsIds.contains(recommendResponse.itemID)) {// adding the recommended to set to prevent duplications
                    recommendItemsIds.add(recommendResponse.itemID);
                    RecommendRsponseDepth recommendRsponseDepth = new RecommendRsponseDepth(recommendResponse.itemID, recommendRsponse.getDepth() + 1);
                    recommendRsponseDepthsStack.add(recommendRsponseDepth);
                }
            }
        }
        recommendOfRecommendResult.recommendationsOfRecommendationResult = result;
        return recommendOfRecommendResult;

    }

}
